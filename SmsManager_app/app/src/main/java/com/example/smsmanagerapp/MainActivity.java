package com.example.smsmanagerapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.xml.transform.URIResolver;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    TextView textView;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button= findViewById(R.id.button1);
        button.setOnClickListener(this);
        textView = findViewById(R.id.textView);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_SMS}, PackageManager.PERMISSION_GRANTED);
    }
public void read_SMS(View view){

    Uri message = Uri.parse("content://sms/");
    ContentResolver cr = getContentResolver();
    Cursor cursor = cr.query(message, null, null, null, null);
    cursor.moveToFirst();
    String toLog = "Contact number : "
            + cursor.getString(cursor
            .getColumnIndexOrThrow("address"))
            + "\n"
            + "msg : "
            + cursor.getString(cursor.getColumnIndexOrThrow("body"))
            + "\n"
            + "ID : "
            + cursor.getString(cursor.getColumnIndexOrThrow("_id"));

   textView.setText(toLog);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        read_SMS(v);

    }
}
